class Test  {

    void DeleteTest(){
        CPatchStr a ("aaaa");
        a.Insert(0, "bbbb ");
        assert (stringMatch(a.ToStr(), "bbbb aaaa"));
        a.Insert(2, " cccc ");
        assert (stringMatch(a.ToStr(), "bb cccc bb aaaa"));
        a.Insert(15, " dddd");
        assert (stringMatch(a.ToStr(), "bb cccc bb aaaa dddd"));
        a.Insert(0, "bbbb ");
        assert (stringMatch(a.ToStr(), "bbbb bb cccc bb aaaa dddd"));
        a.Insert(2, " cccc ");
        assert (stringMatch(a.ToStr(), "bb cccc bb bb cccc bb aaaa dddd"));
        a.Insert(31, " dddd");
        assert (stringMatch(a.ToStr(), "bb cccc bb bb cccc bb aaaa dddd dddd"));
        a.Insert(0, "xxxx ").Insert(41, " yyyy");
        assert (stringMatch(a.ToStr(), "xxxx bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        a.Insert(1, "1111");
        assert (stringMatch(a.ToStr(), "x1111xxx bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        a.Insert(8, "8888");
        assert (stringMatch(a.ToStr(), "x1111xxx8888 bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        a.Insert(13, "1313");
        assert (stringMatch(a.ToStr(), "x1111xxx8888 1313bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        a.Insert(7, a.SubStr(0, 23));
        assert (stringMatch(a.ToStr(), "x1111xxx1111xxx8888 1313bb cccx8888 1313bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        a.Append("asdfasdf65654564asf");
        a.Append(a);
        a.Append(a);

        string s = "x1111xxx1111xxx8888 1313bb cccx8888 1313bb cccc bb bb cccc bb aaaa dddd dddd yyyyasdfasdf65654564asfx1111xxx1111xxx8888 1313bb cccx8888 1313bb cccc bb bb cccc bb aaaa dddd dddd yyyyasdfasdf65654564asfx1111xxx1111xxx8888 1313bb cccx8888 1313bb cccc bb bb cccc bb aaaa dddd dddd yyyyasdfasdf65654564asfx1111xxx1111xxx8888 1313bb cccx8888 1313bb cccc bb bb cccc bb aaaa dddd dddd yyyyasdfasdf65654564asf";

        a.Delete(25,1);
        s.erase(25,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(26,1);
        s.erase(26,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(34,1);
        s.erase(34,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(12,1);
        s.erase(12,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(34,1);
        s.erase(34,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(6,1);
        s.erase(6,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(18,1);
        s.erase(18,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(36,1);
        s.erase(36,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(17,1);
        s.erase(17,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(5,1);
        s.erase(5,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(35,1);
        s.erase(35,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(22,1);
        s.erase(22,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(1,1);
        s.erase(1,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(14,1);
        s.erase(14,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(6,1);
        s.erase(6,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(0,1);
        s.erase(0,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(9,1);
        s.erase(9,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(36,1);
        s.erase(36,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(37,1);
        s.erase(37,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(11,1);
        s.erase(11,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(37,1);
        s.erase(37,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(35,1);
        s.erase(35,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(2,1);
        s.erase(2,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(38,1);
        s.erase(38,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(25,1);
        s.erase(25,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(5,1);
        s.erase(5,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(15,1);
        s.erase(15,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(40,1);
        s.erase(40,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(31,1);
        s.erase(31,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(37,1);
        s.erase(37,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(21,1);
        s.erase(21,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(12,1);
        s.erase(12,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(25,1);
        s.erase(25,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(4,1);
        s.erase(4,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(39,1);
        s.erase(39,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(38,1);
        s.erase(38,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(31,1);
        s.erase(31,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(22,1);
        s.erase(22,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(31,1);
        s.erase(31,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(10,1);
        s.erase(10,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(19,1);
        s.erase(19,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(23,1);
        s.erase(23,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(37,1);
        s.erase(37,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(24,1);
        s.erase(24,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(13,1);
        s.erase(13,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(25,1);
        s.erase(25,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(19,1);
        s.erase(19,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(36,1);
        s.erase(36,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(36,1);
        s.erase(36,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(5,1);
        s.erase(5,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(25,1);
        s.erase(25,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(32,1);
        s.erase(32,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(24,1);
        s.erase(24,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(26,1);
        s.erase(26,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(0,1);
        s.erase(0,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(31,1);
        s.erase(31,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(21,1);
        s.erase(21,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(10,1);
        s.erase(10,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(4,1);
        s.erase(4,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(15,1);
        s.erase(15,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(29,1);
        s.erase(29,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(2,1);
        s.erase(2,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(16,1);
        s.erase(16,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(20,1);
        s.erase(20,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(28,1);
        s.erase(28,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(7,1);
        s.erase(7,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(23,1);
        s.erase(23,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(10,1);
        s.erase(10,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(12,1);
        s.erase(12,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(21,1);
        s.erase(21,1);
        assert(stringMatch(a.ToStr(), s.c_str()));

        a.Delete(13,6);
        s.erase(13,6);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(4,16);
        s.erase(4,16);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(20,4);
        s.erase(20,4);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(15,4);
        s.erase(15,4);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(19,11);
        s.erase(19,11);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(0,17);
        s.erase(0,17);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(12,3);
        s.erase(12,3);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(18,14);
        s.erase(18,14);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(20,12);
        s.erase(20,12);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(12,6);
        s.erase(12,6);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(1,1);
        s.erase(1,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(18,17);
        s.erase(18,17);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(8,9);
        s.erase(8,9);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(6,1);
        s.erase(6,1);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(13,4);
        s.erase(13,4);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(8,15);
        s.erase(8,15);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(15,12);
        s.erase(15,12);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(18,18);
        s.erase(18,18);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(4,20);
        s.erase(4,20);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(11,5);
        s.erase(11,5);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(9,5);
        s.erase(9,5);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(18,19);
        s.erase(18,19);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(10,3);
        s.erase(10,3);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(9,2);
        s.erase(9,2);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(4,9);
        s.erase(4,9);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(15,2);
        s.erase(15,2);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(12,2);
        s.erase(12,2);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(17,4);
        s.erase(17,4);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(19,11);
        s.erase(19,11);
        assert(stringMatch(a.ToStr(), s.c_str()));
        a.Delete(6,18);
        s.erase(6,18);
        assert(stringMatch(a.ToStr(), s.c_str()));
    }
    void MC_getStrTest() {

        CPatchStr a;
        a = "aaaa"; a.Append(" bbbb"); a.Append(" cccc"); a.Append(" dddd");
        a.Append(" eeee"); a.Append(" ffff"); a.Append(" gggg");
        assert (stringMatch(a.ToStr(), "aaaa bbbb cccc dddd eeee ffff gggg"));
        cout << "\033[1;42m Test MC_GetStr 1 dokončen \033[0m" << endl << endl;
        a.Insert(0, "1111 ");
        assert (stringMatch(a.ToStr(), "1111 aaaa bbbb cccc dddd eeee ffff gggg"));
        cout << "\033[1;42m Test MC_GetStr 2 dokončen \033[0m" << endl << endl;
        a = "aaaa"; a.Append(" bbbb"); a.Append(" cccc"); a.Append(" dddd");
        a.Append(" eeee"); a.Append(" ffff"); a.Append(" gggg");
        a.Insert(3, "1111");
        assert (stringMatch(a.ToStr(), "aaa1111a bbbb cccc dddd eeee ffff gggg"));
        cout << "\033[1;42m Test MC_GetStr 3 dokončen \033[0m" << endl << endl;
        a = "aaaa"; a.Append(" bbbb"); a.Append(" cccc"); a.Append(" dddd");
        a.Append(" eeee"); a.Append(" ffff"); a.Append(" gggg");
        a.Insert(4, "1111");
        assert (stringMatch(a.ToStr(), "aaaa1111 bbbb cccc dddd eeee ffff gggg"));
        cout << "\033[1;42m Test MC_GetStr 4 dokončen \033[0m" << endl << endl;
        a = "aaaa"; a.Append(" bbbb"); a.Append(" cccc"); a.Append(" dddd");
        a.Append(" eeee"); a.Append(" ffff"); a.Append(" gggg");
        a.Insert(5, "1111");
        assert (stringMatch(a.ToStr(), "aaaa 1111bbbb cccc dddd eeee ffff gggg"));
        cout << "\033[1;42m Test MC_GetStr 4 dokončen \033[0m" << endl << endl;
        a = "aaaa"; a.Append(" bbbb"); a.Append(" cccc"); a.Append(" dddd");
        a.Insert(19, "1111");
        assert (stringMatch(a.ToStr(), "aaaa bbbb cccc dddd1111"));
        cout << "\033[1;42m Test MC_GetStr 5 dokončen \033[0m" << endl << endl;
        cout << "\033[1;42m\n\n      testy mezních hodnot dokončeny     \n\033[0m" << endl << endl;
    }
    void MC_otherTests() {

        CPatchStr a("abcdefghijklmnopqrstuvwxyz");
        a.Insert(10, a.Delete(2, 15));
        assert (stringMatch(a.ToStr(), "abrstuvwxyabrstuvwxyzz"));
        cout << "\033[1;42m Test MC_Others 1 dokončen \033[0m" << endl << endl;
        a.Append(a.SubStr(10, 12));
        assert (stringMatch(a.ToStr(), "abrstuvwxyabrstuvwxyzzabrstuvwxyzz"));
        cout << "\033[1;42m Test MC_Others 2 dokončen \033[0m" << endl << endl;
        CPatchStr b("123456");
        a.Append(b);
        assert (stringMatch(a.ToStr(), "abrstuvwxyabrstuvwxyzzabrstuvwxyzz123456"));
        cout << "\033[1;42m Test MC_Others 3 dokončen \033[0m" << endl << endl;
        b.Delete(0, 6);
        assert (stringMatch(b.ToStr(), ""));
        cout << "\033[1;42m Test MC_Others 4 dokončen \033[0m" << endl << endl;
        a.Append(b);
        assert (stringMatch(a.ToStr(), "abrstuvwxyabrstuvwxyzzabrstuvwxyzz123456"));
        cout << "\033[1;42m Test MC_Others 5 dokončen \033[0m" << endl << endl;
        a.Delete(0, 40);
        assert (stringMatch(a.ToStr(), ""));
        cout << "\033[1;42m Test MC_Others 6 dokončen \033[0m" << endl << endl;
        cout << "\033[1;42m\n\n      další testy dokončeny     \n\033[0m" << endl << endl;
    }
    void MC_allocationTest() {

        CPatchStr a("");
        const int RAND_LEN = 30000;
        char  randSeed [RAND_LEN];
        for ( int i = 0; i < RAND_LEN - 1; i ++ )
        {
            randSeed[i] = 'a' + (int)(rand () * 26.0 / RAND_MAX );
        }
        randSeed[RAND_LEN - 1] = 0;
        for(int i = 0; i != 100; ++i)
        {
            a.Append(randSeed);
        }
        cout << "\033[1;42m\n\n      testy alokace dokončeny     \n\033[0m" << endl << endl;
    }
    void MC_deleteTest() {

        CPatchStr a("abcdefghijklmnopqrstuvwxyz");

//odebírání v rámci jedné části
//odebírání ze začátku
        a.Delete(0, 3);
        assert (stringMatch(a.ToStr(), "defghijklmnopqrstuvwxyz"));
        cout << "\033[1;42m Test MC_Delete 1 dokončen \033[0m" << endl << endl;
//odebírání z prostředka

        a.Delete(5, 5);
        assert (stringMatch(a.ToStr(), "defghnopqrstuvwxyz"));
        cout << "\033[1;42m Test MC_Delete 2 dokončen \033[0m" << endl << endl;
//odebírání z konce
        a.Delete(12, 6);
        assert (stringMatch(a.ToStr(), "defghnopqrst"));
        cout << "\033[1;42m Test MC_Delete 3 dokončen \033[0m" << endl << endl;
//odebírání napříč částmi
        a = "aaaa";
        a.Append(" bbbb");
        a.Append(" cccc");
        a.Append(" dddd");
        a.Append(" eeee");
        a.Append(" ffff");
        a.Append(" gggg");
        a.Delete(2, 5);
        assert (stringMatch(a.ToStr(), "aabb cccc dddd eeee ffff gggg"));
        cout << "\033[1;42m Test MC_Delete 4 dokončen \033[0m" << endl << endl;
        a.Delete(1, 16);
        assert (stringMatch(a.ToStr(), "aee ffff gggg"));
        cout << "\033[1;42m Test MC_Delete 5 dokončen \033[0m" << endl << endl;
        a.Append(" bbbb");
        a.Append(" cccc");
        a.Append(" dddd");
        a.Append(" eeee");
        //cout << "současné a: " << a << endl;
        assert (stringMatch(a.ToStr(), "aee ffff gggg bbbb cccc dddd eeee"));
        cout << "\033[1;42m Test MC_Delete 6 dokončen \033[0m" << endl << endl;
        a.Delete(2, 12).Delete(5, 10).Delete(0, 8);
        assert (stringMatch(a.ToStr(), "eee"));
        cout << "\033[1;42m Test MC_Delete 7 dokončen \033[0m" << endl << endl;
//odebrání prázdné části
        a.Delete(0, 0).Delete(1, 0).Delete(2, 0);
        assert (stringMatch(a.ToStr(), "eee"));
        cout << "\033[1;42m Test MC_Delete 8 dokončen \033[0m" << endl << endl;
        cout << "\033[1;42m\n\n      testy mazání dokončeny     \n\033[0m" << endl << endl;
    }
    void MC_assignTest() {

//přiřazení prázdného řetězce do prázdného řetězce
        CPatchStr a, b;
        a = "";
        b = "";
        a = b;
        a = a;
        assert (stringMatch(a.ToStr(), ""));
        cout << "\033[1;42m Test MC_Assign 1 dokončen \033[0m" << endl << endl;
        assert (stringMatch(b.ToStr(), ""));
        cout << "\033[1;42m Test MC_Assign 2 dokončen \033[0m" << endl << endl;
//přiřazení v rámci zakládání objektu
        a = "aaaa";
        CPatchStr c = a;
        assert (stringMatch(c.ToStr(), "aaaa"));
        cout << "\033[1;42m Test MC_Assign 3 dokončen \033[0m" << endl << endl;
        c = c;
        assert (stringMatch(c.ToStr(), "aaaa"));
        cout << "\033[1;42m Test MC_Assign 4 dokončen \033[0m" << endl << endl;
//řetězení přiřazení
        a = b = c = "cccc";
        assert (stringMatch(a.ToStr(), "cccc"));
        cout << "\033[1;42m Test MC_Assign 5 dokončen \033[0m" << endl << endl;
        assert (stringMatch(b.ToStr(), "cccc"));
        cout << "\033[1;42m Test MC_Assign 6 dokončen \033[0m" << endl << endl;
        assert (stringMatch(c.ToStr(), "cccc"));
        cout << "\033[1;42m Test MC_Assign 7 dokončen \033[0m" << endl << endl;
//přiřazení novho objektu do existujícího
        c = CPatchStr();
        assert (stringMatch(c.ToStr(), ""));
        cout << "\033[1;42m Test MC_Assign 8 dokončen \033[0m" << endl << endl;
        cout << "\033[1;42m\n\n      testy přiřazování dokončeny     \n\033[0m" << endl << endl;
    }
    void MC_insertTest() {

        CPatchStr a ("aaaa");
//vkládání na začátek části řetězce
        a.Insert(0, "bbbb ");
        assert (stringMatch(a.ToStr(), "bbbb aaaa"));
        cout << "\033[1;42m Test MC_Insert 1 dokončen \033[0m" << endl << endl;
//vkládání doprostřed části řetězce
        a.Insert(2, " cccc ");
        assert (stringMatch(a.ToStr(), "bb cccc bb aaaa"));
        cout << "\033[1;42m Test MC_Insert 2 dokončen \033[0m" << endl << endl;
//vkládání na konec části řetězce
        a.Insert(15, " dddd");
        assert (stringMatch(a.ToStr(), "bb cccc bb aaaa dddd"));
        cout << "\033[1;42m Test MC_Insert 3 dokončen \033[0m" << endl << endl;
//vkládání na začátek části řetězce
        a.Insert(0, "bbbb ");
        assert (stringMatch(a.ToStr(), "bbbb bb cccc bb aaaa dddd"));
        cout << "\033[1;42m Test MC_Insert 4 dokončen \033[0m" << endl << endl;
//vkládání doprostřed části řetězce
        a.Insert(2, " cccc ");
        assert (stringMatch(a.ToStr(), "bb cccc bb bb cccc bb aaaa dddd"));
        cout << "\033[1;42m Test MC_Insert 5 dokončen \033[0m" << endl << endl;
//vkládání na konec části řetězce
        a.Insert(31, " dddd");
        assert (stringMatch(a.ToStr(), "bb cccc bb bb cccc bb aaaa dddd dddd"));
        cout << "\033[1;42m Test MC_Insert 6 dokončen \033[0m" << endl << endl;
//řetězení vkládání 1) na začátek, 2) na konec
        a.Insert(0, "xxxx ").Insert(41, " yyyy");
        assert (stringMatch(a.ToStr(), "xxxx bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        cout << "\033[1;42m Test MC_Insert 7 dokončen \033[0m" << endl << endl;
//náhodně určené vkládání
        a.Insert(1, "1111");
        assert (stringMatch(a.ToStr(), "x1111xxx bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        cout << "\033[1;42m Test MC_Insert 8 dokončen \033[0m" << endl << endl;
        a.Insert(8, "8888");
        cout<<a.ToStr()<<endl;
        assert (stringMatch(a.ToStr(), "x1111xxx8888 bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        cout << "\033[1;42m Test MC_Insert 9 dokončen \033[0m" << endl << endl;
        a.Insert(13, "1313");
        assert (stringMatch(a.ToStr(), "x1111xxx8888 1313bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        cout << "\033[1;42m Test MC_Insert 10 dokončen \033[0m" << endl << endl;
//náročnější testy vkládající jiný řetězec
        a.Insert(7, a.SubStr(0, 23));
        assert (stringMatch(a.ToStr(), "x1111xxx1111xxx8888 1313bb cccx8888 1313bb cccc bb bb cccc bb aaaa dddd dddd yyyy"));
        cout << "\033[1;42m Test MC_Insert 11 dokončen \033[0m" << endl << endl;
        cout << "\033[1;42m\n\n      testy vkládání dokončeny     \n\033[0m" << endl << endl;
    }
    void MC_appendTest() {

//stadnartní sloučení tří řetězců
        CPatchStr a ("aaaa");
        CPatchStr b;
        b = " bbbb";
        CPatchStr c (" cccc ") ;
        a.Append(b).Append(c);
        assert (stringMatch( a.ToStr(), "aaaa bbbb cccc "));
        cout << "\033[1;42m Test MC_Append 1 dokončen \033[0m" << endl << endl;
//připojení řetězce do sebe sama
        a.Append(a);
        assert (stringMatch(a.ToStr(), "aaaa bbbb cccc aaaa bbbb cccc "));
        cout << "\033[1;42m Test MC_Append 2 dokončen \033[0m" << endl << endl;
//připojení nového, prázdného řetězce
        a.Append(CPatchStr()).Append(CPatchStr()).Append(CPatchStr()).Append(CPatchStr());
        assert (stringMatch(a.ToStr(), "aaaa bbbb cccc aaaa bbbb cccc "));
        cout << "\033[1;42m Test MC_Append 3 dokončen \033[0m" << endl << endl;
//připojení prázdného řetězce do přázdnéoh řetězce
        CPatchStr e;
        e.Append(CPatchStr()).Append(CPatchStr()).Append(CPatchStr()).Append(CPatchStr());
        assert (stringMatch(e.ToStr(), ""));
        cout << "\033[1;42m Test MC_Append 4 dokončen \033[0m" << endl << endl;
//připojení prázdného řetězce do sebe sama
        e.Append(e).Append(e).Append(e);
        assert (stringMatch(e.ToStr(), ""));
        cout << "\033[1;42m Test MC_Append 5 dokončen \033[0m" << endl << endl;
//připojení běžného řetězce k prázdnému řetězci
        e.Append(a);
        assert (stringMatch(e.ToStr(), "aaaa bbbb cccc aaaa bbbb cccc "));
        cout << "\033[1;42m Test MC_Append 6 dokončen \033[0m" << endl << endl;
        cout << "\033[1;42m\n\n      testy připojování dokončeny     \n\033[0m" << endl << endl;
    }
public:
    void tests() {

        MC_appendTest();
        cout<<"DONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEN"
              "DONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONE"
              "DOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONED"
              "ONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEND"
              "ONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONED1"<<endl;
        MC_insertTest();
        cout<<"DONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEN"
              "DONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONE"
              "DOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONED"
              "ONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEND"
              "ONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONED2"<<endl;
        MC_assignTest();
        cout<<"DONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEN"
              "DONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONE"
              "DOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONED"
              "ONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEND"
              "ONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONED3"<<endl;
        MC_deleteTest();
        cout<<"DONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEN"
              "DONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONE"
              "DOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONED"
              "ONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEND"
              "ONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONED4"<<endl;
        MC_allocationTest();
        cout<<"DONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEN"
              "DONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONE"
              "DOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONED"
              "ONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEND"
              "ONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONED5"<<endl;
        MC_otherTests();
        cout<<"DONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEN"
              "DONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONE"
              "DOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONED"
              "ONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEND"
              "ONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONED6"<<endl;
        MC_getStrTest();
        cout<<"DONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEN"
              "DONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONE"
              "DOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONED"
              "ONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEND"
              "ONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONED7"<<endl;
        DeleteTest();
        cout<<"DONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEN"
              "DONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONE"
              "DOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONED"
              "ONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOEND"
              "ONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONEDOENDONEDONED8"<<endl;
        cout << "\033[1;42m\n\n\n      MC testy dokončeny     \n\n\033[0m" << endl << endl;
    }
};