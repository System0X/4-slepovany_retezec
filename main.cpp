#include <utility>

#ifndef __PROGTEST__

#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <memory>

using namespace std;

class InvalidIndexException {
};

#endif /* __PROGTEST__ */

class String {
private:
    char *_word= nullptr;
public:
    explicit String(const char *word) {
        _word = new char[strlen(word)];
        strcpy(_word, word);
    }
    void setWord(const char *word){
        if(_word!= nullptr){
            delete [] _word;
        }

        _word = new char[strlen(word)+1];
        strcpy(_word, word);
    }
    char *getString() const {
        return _word;
    }

    int getSize() const {
        return strlen(_word);
    }

    ~String(){
        delete [] _word;
    }
};

class Word {
public:
    shared_ptr<String> _word ;
    int _offset = 0;
    int _len = 0;

    Word(shared_ptr<String> word, int offset, int len) : _offset(offset), _len(len) {
        _word=word;
    }
    Word(const Word & temp){
        *this=temp;
        _offset=temp._offset;
        _len=temp._len;
    }
    Word(){
        _word=make_shared<String>("");
    }
    char* getWord(){
        char* tmp=_word->getString();

        char* newChar=new char[_len];
        int b=0;
            for(int i=_offset;i<_len;i++,b++){
                newChar[b]=tmp[i];
            }
        return newChar;
    }

    Word &operator=(const Word& oldWord)=default;
    void makeNew(shared_ptr<String> word, int offset, int len) {
        _offset = offset;
        _len = len;
        _word = word;
    }
    ~Word(){
        //_word= nullptr;
    }
};

class CPatchStr {
private:
    Word *_words=new Word[1];
    int _size = 1;
    int _usedSize = 0;
    int _numberOfChars = 0;

    void willUse(int size) {
        if (_usedSize + size < _size) {
            return;
        } else {

            Word *tmp = new Word[(_usedSize + size)*2 ];//todo make bigger alloc
            for(int i=0;i<_usedSize;i++){
                tmp[i]=_words[i];
            }
            _size += size + 1;
            _words = tmp;
        }

    }
    int whereToPut(int &where)const{
        bool isThere=false;
        int i=0;
        where-=_words[i]._len;
        while(where>0){
            i++;
            where-=_words[i]._len;

        }
        return i;
    }
public:
    CPatchStr(void) = default;

    CPatchStr(const char *str) {
        if(strlen(str)==0){
            return;
        }
        shared_ptr<String> word;
        word=make_shared<String>(str);
        willUse(1);
        _words[_usedSize].makeNew(word,0,strlen(str));
        _usedSize++;
        _numberOfChars+=strlen(str);
    }

    CPatchStr(const CPatchStr &copyString) {
        willUse(copyString._usedSize);
        int b=0;
        for(int i=_usedSize;i<(copyString._usedSize+_usedSize);i++,b++){
            _words[i]=copyString._words[b];
        }
        _usedSize+=copyString._usedSize;
        _numberOfChars+=copyString._numberOfChars;
    }

    CPatchStr operator =(const char * str){
        if(strlen(str)==0){
            _usedSize=0;
            _numberOfChars=0;
            return *this;
        }
        _usedSize=0;
        _numberOfChars=0;
        shared_ptr<String> word;
        word=make_shared<String>(str);
        willUse(1);
        _words[_usedSize].makeNew(word,0,strlen(str));
        _usedSize++;
        _numberOfChars+=strlen(str);
        return *this;
    }
    CPatchStr operator =(const CPatchStr & newPatch){
        if(&newPatch==this){

            return *this;
        }
        if(newPatch._numberOfChars==0){
            _numberOfChars=0;
            _size=0;
            _usedSize=0;
            return *this;
        }
        _numberOfChars=0;
        delete[] _words;
        _size=0;
        _usedSize=0;
        willUse(newPatch._usedSize);
        for(int i=0;i<newPatch._usedSize;i++){
            _words[i]=newPatch._words[i];
        }
        _usedSize=newPatch._usedSize;
        _numberOfChars=newPatch._numberOfChars;
        return *this;
    }
    CPatchStr operator +=(const CPatchStr & newPatch){
        willUse(newPatch._usedSize);
        int b=0;
        for(int i=_usedSize;i<(newPatch._usedSize+_usedSize);i++,b++){
            _words[i]=newPatch._words[b];
        }
        _usedSize+=newPatch._usedSize;
        _numberOfChars+=newPatch._numberOfChars;
        return *this;
    }
    CPatchStr operator +=(const Word & newWord){

        willUse(1);
        _words[_usedSize]=newWord;
        _usedSize++;
        _numberOfChars+=newWord._len;
        return *this;
    }


    CPatchStr SubStr(size_t from,
                     size_t len) const{
        if(from+len>_numberOfChars){
            throw InvalidIndexException();
        }
        int endbit=from+len;
        int startbit=from;
        int start=whereToPut(startbit);
        int end=whereToPut(endbit);
        CPatchStr patch;
        patch.willUse(end-start+1);
        patch._usedSize=end-start+1;
startbit+=_words[start]._len;
endbit+=_words[end]._len;
        int c=1;
        if(start!=end) {
            patch._words[0].makeNew(_words[start]._word, startbit, _words[start]._len-startbit);

        }else {
            patch._words[0].makeNew(_words[start]._word, startbit, len);

        }
        patch._numberOfChars=len;
        for(int i=start+1;i<=end;i++,c++){
            if(end!=i) {
                patch._words[c].makeNew(_words[i]._word, _words[i]._offset,_words[i]._len);

            }else {
                patch._words[c].makeNew(_words[i]._word, _words[i]._offset,endbit);

            }
        }

        return patch;


    }

    CPatchStr &Append(const CPatchStr &src){
        *this+=src;

        return *this;
    }

    CPatchStr &Insert(size_t pos,
                      const CPatchStr &src){
        if(pos>_numberOfChars){
            throw InvalidIndexException();
        }
        CPatchStr newStr;
        int startbit=pos;
        int start=whereToPut(startbit);
        startbit+=_words[start]._len;
        newStr.willUse(_usedSize+src._usedSize+3);
        for(int i=0;i<start;i++){
            newStr+=_words[i];
        }

        Word newWord(_words[start]._word,_words[start]._offset,startbit);

        newStr+=newWord;

        newStr+=src;


            Word newNewWorld(_words[start]._word, startbit+_words[start]._offset, _words[start]._len -startbit);
            newStr += newNewWorld;





        for(int i=start+1;i<_usedSize;i++){
            newStr+=_words[i];
        }
        *this=newStr;


        return *this;

    }

    CPatchStr &Delete(size_t from,
                      size_t len){
        if(from+len>_numberOfChars){
            throw InvalidIndexException();
        }

        CPatchStr newStr;
        int startbit=from;
        int endBit=from+len;
        int start=whereToPut(startbit);
        int end=whereToPut(endBit);
        startbit+=_words[start]._len;
        endBit+=_words[end]._len;

        newStr.willUse(_usedSize+3);
        for(int i=0;i<start;i++){
            newStr+=_words[i];
        }

            Word newWord(_words[start]._word, _words[start]._offset, startbit);
            newStr += newWord;

        if(_words[end]._len - endBit>0) {
            Word newNewWorld(_words[end]._word, endBit + _words[end]._offset, _words[end]._len - endBit);
            newStr += newNewWorld;
        }
        for(int i=end+1;i<_usedSize;i++){
            newStr+=_words[i];
        }

        *this=newStr;

        return *this;
    }

    char *ToStr(void) const {
        char * retchar;

        if (_words == nullptr) {
            retchar=new char[1];
            retchar[0]='\0';
            return retchar;
        }
        int b=0;
        retchar=new char[_numberOfChars+1];
        for(int i=0;i<_usedSize;i++){
            char* tmp=_words[i]._word->getString();
            for(int c=_words[i]._offset;c<_words[i]._len+_words[i]._offset;c++,b++){
                retchar[b]=tmp[c];
            }
        }
        retchar[_numberOfChars]='\0';
        return retchar;
    }

//    ~CPatchStr(){
//        delete[]_words;
//    }
};

#ifndef __PROGTEST__

bool stringMatch(char *str,
                 const char *expected) {
    bool res = strcmp(str, expected) == 0;
    delete[] str;
    return res;
}
int main(void) {

    char tmpStr[100];

    CPatchStr a("test");
    assert (stringMatch(a.ToStr(), "test"));
    strncpy(tmpStr, " da", sizeof(tmpStr));
    a.Append(tmpStr);
    assert (stringMatch(a.ToStr(), "test da"));
    strncpy(tmpStr, "ta", sizeof(tmpStr));
    a.Append(tmpStr);
    assert (stringMatch(a.ToStr(), "test data"));
    strncpy(tmpStr, "foo text", sizeof(tmpStr));
    CPatchStr b(tmpStr);
    assert (stringMatch(b.ToStr(), "foo text"));
    CPatchStr c(a);
    assert (stringMatch(c.ToStr(), "test data"));
    CPatchStr d(a.SubStr(3, 5));
    assert (stringMatch(d.ToStr(), "t dat"));
    d.Append(b);
    assert (stringMatch(d.ToStr(), "t datfoo text"));
    d.Append(b.SubStr(3, 4));
    assert (stringMatch(d.ToStr(), "t datfoo text tex"));
    c.Append(d);
    assert (stringMatch(c.ToStr(), "test datat datfoo text tex"));
    c.Append(c);
    assert (stringMatch(c.ToStr(), "test datat datfoo text textest datat datfoo text tex"));
    d.Insert(2, c.SubStr(6, 9));
    assert (stringMatch(d.ToStr(), "t atat datfdatfoo text tex"));
    b = "abcdefgh";
    assert (stringMatch(b.ToStr(), "abcdefgh"));
    assert (stringMatch(b.ToStr(), "abcdefgh"));
    assert (stringMatch(d.ToStr(), "t atat datfdatfoo text tex"));
    assert (stringMatch(d.SubStr(4, 8).ToStr(), "at datfd"));
    assert (stringMatch(b.SubStr(2, 6).ToStr(), "cdefgh"));
    try {
        b.SubStr(2, 7).ToStr();
        assert ("Exception not thrown" == NULL);
    }
    catch (InvalidIndexException &e) {
    }
    catch (...) {
        assert ("Invalid exception thrown" == NULL);
    }

    a.Delete(3, 5);

    assert (stringMatch(a.ToStr(), "tesa"));

    return 0;
}


#endif /* __PROGTEST__ */
